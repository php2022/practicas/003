<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <style>
            form label,form input,form button{
                margin:4px;
            }
        </style>
    </head>
    <body>
        <form method="post">
            <label for="fname">Introduce radio:</label><br>
            <input type="text" id="radio" name="radio" value=""><br>
            <button type="submit" value="">Enviar</button>
        </form>
        <?php
        if($_POST){
            $radio=$_POST["radio"]; 
            $longi=2*M_PI*$radio;
            echo "La longitud del círculo es $longi<br>";
            $area=M_PI*pow($radio,2);
            echo "El área del círculo es $area<br>";
            $volu= 4/3*M_PI*pow($radio,3);
            echo "El volumen del círculo es $volu";
        }
        ?>
    </body>
</html>
